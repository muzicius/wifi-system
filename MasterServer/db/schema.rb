# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160818161345) do

  create_table "actions", force: :cascade do |t|
    t.string  "action_type", limit: 255, null: false
    t.string  "ip",          limit: 255
    t.integer "bandwidth",   limit: 4
    t.integer "minutes",     limit: 4
    t.string  "url",         limit: 255
  end

  create_table "authorized_apps", force: :cascade do |t|
    t.string "token", limit: 255, null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string  "phone",           limit: 255
    t.integer "speed_limit_kbs", limit: 4
    t.integer "group_policy_id", limit: 4
    t.boolean "is_banned",                   default: false
  end

  add_index "clients", ["group_policy_id"], name: "index_clients_on_group_policy_id", using: :btree

  create_table "forbidden_ips", force: :cascade do |t|
    t.string "ip",     limit: 255, null: false
    t.string "domain", limit: 255
  end

  create_table "group_policies", force: :cascade do |t|
    t.string  "name",            limit: 255,                 null: false
    t.integer "speed_limit_kbs", limit: 4
    t.boolean "is_banned",                   default: false
  end

  create_table "group_to_forbidden_ips", force: :cascade do |t|
    t.integer "group_policy_id", limit: 4
    t.integer "forbidden_ip_id", limit: 4
  end

  add_index "group_to_forbidden_ips", ["forbidden_ip_id"], name: "index_group_to_forbidden_ips_on_forbidden_ip_id", using: :btree
  add_index "group_to_forbidden_ips", ["group_policy_id"], name: "index_group_to_forbidden_ips_on_group_policy_id", using: :btree

  create_table "repost_check_pools", force: :cascade do |t|
    t.integer  "vk_id",      limit: 4
    t.integer  "post_id",    limit: 4
    t.integer  "owner_id",   limit: 4
    t.boolean  "reposted"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  create_table "session_auths", force: :cascade do |t|
    t.integer  "session_id",         limit: 4
    t.integer  "code",               limit: 4,             null: false
    t.datetime "code_start_time",                          null: false
    t.datetime "code_end_time",                            null: false
    t.integer  "sms_counter",        limit: 4, default: 0
    t.datetime "last_sms_sent_time"
  end

  add_index "session_auths", ["session_id"], name: "index_session_auths_on_session_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.integer  "client_id",      limit: 4
    t.string   "ip",             limit: 255,                 null: false
    t.datetime "beginning_time"
    t.datetime "end_time"
    t.string   "status",         limit: 255
    t.string   "code",           limit: 255, default: "lol", null: false
  end

  add_index "sessions", ["client_id"], name: "index_sessions_on_client_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string  "email",           limit: 255, null: false
    t.string  "organization",    limit: 255
    t.integer "role_id",         limit: 4
    t.string  "password_digest", limit: 255
    t.string  "remember_digest", limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree

  create_table "vk_clients", force: :cascade do |t|
    t.string   "url",                    limit: 255
    t.string   "username",               limit: 255
    t.string   "is_banned",              limit: 255
    t.integer  "clients_id",             limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "email",                  limit: 255, default: ""
    t.string   "encrypted_password",     limit: 255, default: ""
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "vk_id",                  limit: 4
  end

  add_index "vk_clients", ["email"], name: "index_vk_clients_on_email", unique: true, using: :btree
  add_index "vk_clients", ["reset_password_token"], name: "index_vk_clients_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "clients", "group_policies"
  add_foreign_key "group_to_forbidden_ips", "forbidden_ips"
  add_foreign_key "group_to_forbidden_ips", "group_policies"
  add_foreign_key "session_auths", "sessions"
  add_foreign_key "sessions", "clients"
  add_foreign_key "users", "roles"
end
