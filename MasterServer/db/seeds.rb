# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
supervisor = Role.create(name: 'Supervisor')
admin= Role.create(name: 'Admin')
pr = Role.create(name:'PR')
group_policy = GroupPolicy.create(name: 'test_group')
group_policy_default = GroupPolicy.create(name: 'default_group', speed_limit_kbs: 500)


forb_ip = ForbiddenIp.create(ip: 'test_ip', domain: 'test_domain')
GroupToForbiddenIp.create(group_policy_id: group_policy.id, forbidden_ip_id: forb_ip.id)


user = User.create(email: 'admin11111@inno.ru', password: 'pass_test', password_confirmation: 'pass_test', role_id:1)
admin = User.create(email: 'ne_admin@inno.ru', password: 'password', password_confirmation: 'password', role_id:2)


