class CreateVkClients < ActiveRecord::Migration
  def change
    create_table :vk_clients do |t|
      t.string :url
      t.string :username
      t.string :is_banned
      t.references :clients, uniqueness: true
      t.timestamps null: false
    end
  end
end
