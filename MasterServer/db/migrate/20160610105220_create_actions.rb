class CreateActions < ActiveRecord::Migration
  def change
    create_table :actions do |t|
      t.string :action_type, null: false
      t.string :ip
      t.integer :bandwidth
      t.integer :minutes
      t.string :url
    end
  end
end
