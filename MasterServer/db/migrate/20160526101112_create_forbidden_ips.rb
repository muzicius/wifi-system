class CreateForbiddenIps < ActiveRecord::Migration
  def change
    create_table :forbidden_ips do |t|
      t.string :ip, null: false, uniqueness: true
      t.string :domain
    end
  end
end
