class CreateGroupPolicies < ActiveRecord::Migration
  def change
    create_table :group_policies do |t|
      t.string :name, null: false, uniqueness: true
      t.integer :speed_limit_kbs, null: true
      t.boolean  :is_banned, default: false
    end
  end
end
