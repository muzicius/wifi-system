class CreateRepostCheckPools < ActiveRecord::Migration
  def change

    create_table :repost_check_pools do |t|
      t.integer :vk_id, primary_key: true
      t.integer :post_id, primary_key: true
      t.integer :owner_id, primary_key: true
      t.boolean :reposted
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
