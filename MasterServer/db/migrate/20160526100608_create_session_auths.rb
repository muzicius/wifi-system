class CreateSessionAuths < ActiveRecord::Migration
  def change
    create_table :session_auths do |t|
      t.belongs_to :session, index: true, foreign_key: true, uniqueness: true
      t.integer :code, null: false
      t.timestamp :code_start_time, null: false
      t.timestamp :code_end_time, null: false
      t.integer :sms_counter, default: 0
      t.timestamp :last_sms_sent_time
    end
  end
end
