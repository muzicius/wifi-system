class CreateGroupToForbiddenIps < ActiveRecord::Migration
  def change
    create_table :group_to_forbidden_ips do |t|
      t.belongs_to :group_policy, index: true, foreign_key: true
      t.belongs_to :forbidden_ip, index: true, foreign_key: true
    end
  end
end
