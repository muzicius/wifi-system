class CreateSessions < ActiveRecord::Migration
  def change
    create_table :sessions do |t|
      t.belongs_to :client, index: true, foreign_key: true
      t.string :ip, null: false
      t.timestamp :beginning_time, null: true
      t.timestamp :end_time, null: true
      t.string :status
      t.string :code, default: "lol", null: false
    end
  end
end
