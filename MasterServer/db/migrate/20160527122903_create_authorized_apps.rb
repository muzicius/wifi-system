class CreateAuthorizedApps < ActiveRecord::Migration
  def change
    create_table :authorized_apps do |t|
      t.string :token, null: false, uniqueness: true
    end
  end
end
