class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, null: false, uniqueness: true
      t.string :password_hash, null: false
      t.string :organization
      t.belongs_to :role, index: true, foreign_key: true
    end
  end
end
