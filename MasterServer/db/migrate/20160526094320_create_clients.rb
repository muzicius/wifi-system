class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :phone, uniqueness: true
      t.integer :speed_limit_kbs, null: true
      t.references :group_policy, index: true, foreign_key: true
      t.boolean :is_banned, default: false
    end
  end
end
