# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

#Load daemons
require_relative '../lib/vk/vkhelper'
require_relative '../lib/vk/vkdaemon'
VKHelper.load_config
# VKDaemon.run!

