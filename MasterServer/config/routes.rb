Rails.application.routes.draw do
  
  namespace :vk_clients do
    get 'omniauth_callbacks/vkontakte'
  end

  get 'sessions/new'
  get 'internet_access' => 'welcome#vk'
  get 'set_post' => 'welcome#set_vk_post'
  # admin panel part
  get 'sessions/clients' => 'welcome#clients'
  get 'sessions/new'
  root 'welcome#index'
  get 'welcome' => 'welcome/index'
  get '/delete/:id' => 'welcome#delete'
  get '/changeSpeed' => 'welcome#changeSpeed'
  get 'stats' =>'stats#index'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'hui' => 'omniauth_callbacks#vkontakte'

  resources :users
  # guests authorization part
  get '/captive/auth' => 'internet_access#phone_template'
  get '/captive/auth/phone' => 'internet_access#send_phone'
  get '/captive/auth/code' => 'internet_access#code_template'
  get '/captive/auth/approve' => 'internet_access#send_code'
  get '/captive' => 'internet_access#not_a_client_template'
  get '/captive/authorized' => 'internet_access#authorized_template', as: 'authorized'
  get '/captive/banned' => 'internet_access#banned_template'
  post '/api/:token/pushCodes' => 'internal#new_session'
  post '/api/:token/pullUpdates' => 'internal#pull_updates'
  post '/api/:token/pushEvents' => 'internal#push_events'

  get '/captive/post' => 'internet_access#set_post_template'
  get '/captive/set_post' => 'internet_access#set_vk_post'

  get '/test' => 'users/index#view'

  
  
  devise_for :vk_clients, :controllers => { :omniauth_callbacks => "vk_clients/omniauth_callbacks" }
  resources :vk_clients, :only => [:index, :destroy]
  # root :to => 'vk_clients#index'
  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  #
  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

