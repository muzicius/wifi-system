require 'rest-client'
require 'json'
require 'yaml'
require 'thread'

module VKDaemon

  INCREASE_TIME_STEP = 15 * 60
  INCREASE_TIME = 60*60*4
  VK_DOMAIN = 'https://api.vk.com/method'

  @@mutex = Mutex.new
  @@owner_id = 1
  @@post_id = 1

  private
  # Основной алгоритм работы демона
  def self.process
    owner_id, post_id = get_post_address
    uids = get_reposted_users(owner_id, post_id)
    waiting = get_waiting_users(owner_id, post_id)

    reposted = uids & waiting

    unless reposted.empty?
      time = Time.now
      RepostCheckPool.where(vk_id: reposted,
                            owner_id: owner_id,
                            post_id: post_id,
                            reposted: false)
                      .where('start_time <= ? AND ? <= end_time', time, time)
                      .update_all(reposted: true)
      increase_time
    end
  end

  # @return [Array] ID пользователей, сделавших репост.
  def self.get_reposted_users(owner_id, post_id, offset: 0)

    request = VK_DOMAIN + "/wall.getReposts?owner_id=#{owner_id}"
    request << "&post_id=#{post_id}&offset#{offset}0&count=100"

    response = RestClient.get request # get request

    response = (JSON.parse response)['response']

    profiles = response['profiles']
    reposted = []
    profiles.each do |profile|
      reposted << profile['uid']
    end

    if response.count == 100;
      reposted += get_reposted_users(owner_id, post_id, offset + 100);
    end

    reposted
  end

  # @return [Array] ID пользователей, ожидающих проверки
  def self.get_waiting_users(owner_id, post_id)
    time_current = Time.now

    response = RepostCheckPool.where('end_time >= ? AND start_time <= ? AND owner_id = ?' +
                                         ' AND post_id = ? AND reposted = 0',
                                     time_current, time_current, owner_id, post_id)
    waiting = []
    response.each do |tuple|
      waiting << tuple.vk_id
    end
    waiting
  end

  # Обновляет пул пользователей
  def self.update_waiting(reposted_users)
    RepostCheckPool.where()
  end

  # Увелчивает время сессии на INCREASE_TIME секунд
  def self.increase_time
    #TODO: Write method
  end

  # @return owner_id, post_id
  def self.get_post_address
    @@mutex.lock
    owner_id = @@owner_id
    post_id = @@post_id
    @@mutex.unlock
    return owner_id, post_id
  end


  public

  # Запускает демона
  def self.run!
    Thread.new do
      puts "Демон ВКонтакте запущен"
      loop do
        begin
          process
        rescue
        end
        sleep 5
      end
    end
  end

  # Добавляет пользователя в очередь на проверку репоста
  # @param [Integer] user_id ID пользователя ВКонтакте
  def self.pool_user(user_id)
    time_start = Time.now
    time_end = time_start + INCREASE_TIME_STEP

    owner_id, post_id = get_post_address

    RepostCheckPool.create({vk_id: user_id,
                            post_id: post_id,
                            owner_id: owner_id,
                            reposted: false,
                            start_time: time_start,
                            end_time: time_end})
  end

  # @return [Bool] true - если пользователь сделал репост, заданного поста.
  def self.has_repost?(user_id)
    time_current = Time.now
    response = RepostCheckPool.where('end_time >= ? AND start_time <= ? AND vk_id = ?',
                                     time_current, time_current, user_id)

    response.count > 0 ? true : false
  end

  def self.set_post(owner_id, post_id)
    @@mutex.lock
    @@owner_id = owner_id
    @@post_id = post_id
    @@mutex.unlock
  end
end







