require 'yaml'
require_relative 'vkdaemon'

module VKHelper
  VK_DOMAIN = 'https://api.vk.com/method'
  CONFIG_PATH = "#{Rails.root}/config/vk_config.yml"

  private

  def self.save_config
    File.write(CONFIG_PATH, @@config.to_yaml)
  end

  def self.load_config
    @@config = YAML::load(File.open(CONFIG_PATH))
    VKDaemon.set_post(@@config['selected_post']['owner_id'],
                      @@config['selected_post']['post_id'])
  end

  public

  # Устанавливает пост для проверки на репост
  # @param [String] post_to_check ссылка на пост ВКонткте
  def self.post_to_check=(link)
    matches = /^*(?<owner_id>-?\d+)_(?<post_id>\d+)\?*$/.match link

    begin
      post_id = matches[:post_id].to_i
      owner_id = matches[:owner_id].to_i

      @@config['selected_post']['post_id'] = post_id
      @@config['selected_post']['owner_id'] = owner_id


      VKDaemon.set_post(owner_id, post_id)
      save_config
    rescue
      raise 'VK_LINK_ERROR'
    end
  end

  def self.get_post_address
    VKDaemon.get_post_address
  end

  # Возвращает информацию о заданном посте
  # @return [Hash] Информация о заданном посте
  # Пример поста:
  # { "id"=>3677,
  #   "from_id" =>-56385969,
  #   "to_id"=> -56385969,
  #   "date"=> 1465562641,
  #   "post_type"=>"post",
  #   "text"=> "Текст поста с HTML разметкой",
  #   "attachment"=>{"type"=>"link",
  #                  "link"=>{"url"=>"http://university.innopolis.ru/news/deal-with-wastes/",
  #                            "title"=>"Российские дети научат роботов бороться с отходами", "description"=>"",
  #                            "image_src"=>"http://cs631221.vk.me/v631221890/2d122/fIe1pwlbpK0.jpg"}
  #                  },
  #   "attachments"=>[{"type"=>"link",
  #                    "link"=>{"url"=>"http://university.innopolis.ru/news/deal-with-wastes/",
  #                     "title"=>"Российские дети научат роботов бороться с отходами",
  #                     "description"=>"",
  #                     "image_src"=>"http://cs631221.vk.me/v631221890/2d122/fIe1pwlbpK0.jpg"}
  #                   }],
  #   "comments"=>{"count"=>0},
  #   "likes"=>{"count"=>17},
  #   "reposts"=>{"count"=>4}
  # }

  def self.get_post_info
    post_id = @@config['selected_post']['post_id']
    owner_id = @@config['selected_post']['owner_id']

    request = VK_DOMAIN + "/wall.getById"
    request << "?posts=#{owner_id}_#{post_id}"
    request << "&extended=1&copy_history_depth=1"

    response = RestClient.get request
    response = (JSON.parse response)['response']["wall"][0]

    response
  end

  # Возвращает конфиг
  # @return [Hash] конфиг в виде хэша
  def self.config
    @@config
  end

  load_config
  TOKEN = @@config['token']
end