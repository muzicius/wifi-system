require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @client = users(:one)
  end
  test "login with invalid information" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "", password: "" }
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end
  test "login with valid information" do
    get login_path
    post login_path, session: {email: @client.email, password: 'password' }
    assert_redirected_to root_url
    follow_redirect!
    assert_template 'welcome/index'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@client)
    assert is_logged_in?
  end
  test "login and logout" do
    get login_path
    post login_path, session: {email: @client.email, password: 'password' }
    assert is_logged_in?
    assert_redirected_to @client
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@client)
    delete logout_path
    assert_not is_logged_in?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,      count: 0
    assert_select "a[href=?]", user_path(@client), count: 0
  end
  test "login with remembering" do
    log_in_as(@client, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@client, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
