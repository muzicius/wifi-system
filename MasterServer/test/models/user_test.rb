require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @client = User.new(email: "usexr@examplecxc.com",
                       password: "foobar", password_confirmation: "foobar", role_id:1)
    puts @client
  end

  test "email addresses should be unique" do
    duplicate_user = @client.dup
    duplicate_user.email = @client.email.upcase
    @client.save
    puts duplicate_user
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @client.email = mixed_case_email
    @client.save
    assert_equal mixed_case_email.downcase, @client.reload.email
  end

  test "password should have a minimum length" do
    @client.password = @client.password_confirmation = "a" * 5
    assert_not @client.valid?
  end
  test "authenticated? should return false for a user with nil digest" do
    assert_not @client.authenticated?('')
  end
end
