class Stat < ActiveRecord::Base
  establish_connection :wifi_stats
  self.table_name = 'wifi_stats.netflow_data'

  def phone
    @session = Session.find_by ip: self.srcaddr
    if @session.nil? then
      @session = Session.find(1)
    end
    @client = Client.find(@session.client_id)
    phone = @client.phone
  end
  filterrific(
      default_filter_params: { sorted_by: 'srcaddr_asc' },
      available_filters: [
          :sorted_by,
          :with_first_gte,
          :with_last_lt
      ]
  )
  scope :sorted_by, lambda { |sort_option|
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    puts sort_option
    case sort_option.to_s
      when /^last_/
        order("netflow_data.last #{ direction }")
      when /^first_/
        order("netflow_data.first #{ direction }")

      when /^srcport_/
        order("netflow_data.srcport #{ direction }")
      when /^srcaddr_/
        order("netflow_data.srcaddr #{ direction }")

      when /^dstaddr_/
        order("netflow_data.dstaddr #{ direction }")
      when /^dstport_/
        order("netflow_data.dstport #{ direction }")
      when /^prot_/
        order("netflow_data.dstport #{ direction }")

      else
        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }
  scope :with_first_gte, lambda { |first|
    @temp = Date._strptime(first, '%m/%d/%Y')
    @unix = Time.new(@temp[:year],@temp[:mon],@temp[:mday]).to_time.to_i
    where('netflow_data.unix_secs >= ?', @unix)
  # netflow_data.unix_secs + (rec.last - rec.first)
  }
  scope :with_last_lt, lambda {|last|
    @temp = Date._strptime(last, '%m/%d/%Y')
    @unix = Time.new(@temp[:year],@temp[:mon],@temp[:mday]).to_time.to_i
    where("netflow_data.unix_secs  + netflow_data.last - netflow_data.first < ?", @unix)
  }


  def self.options_for_sorted_by
    [
        ['Последнее обновление потока(самое давнее)', 'last_asc'],
        ['Последнее обновление потока(самое свежее)', 'last_desc'],
        ['IP назначения(с конца)', 'dstaddr_desc'],
        ['IP назначения(с начала)', 'dstaddr_asc'],
        ['IP пользователя(с конца)', 'srcaddr_desc'],
        ['IP пользователя(с начала)', 'srcaddr_asc']
    ]
  end
end
