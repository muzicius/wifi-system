class User < ActiveRecord::Base


  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  attr_accessor :remember_token

  validates :role_id,
            presence: true

  validates :password,
            presence: true,
            length: {minimum: 6}

  validates :email, presence: true,
            length: {maximum: 255},
            format: {with: VALID_EMAIL_REGEX},
            uniqueness: {case_sensitive: false}




  before_save do
    self.email = self.email.downcase
  end

  has_secure_password

  # Хеширует строку
  # @param [String] string - строка для хеширования
  # @return [String] хеш строки.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Создает новый токен для пользователя
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Запоминает токен пользователя
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end


  # @return [Boolean] true, если токен совпадает с хешем
  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # "Забывает" пользователя
  def forget
    update_attribute(:remember_digest, nil)
  end



end
