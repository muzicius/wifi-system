class VkClient < ActiveRecord::Base
 
    devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :omniauthable,
         :omniauth_providers => [:vkontakte]
         
         # Setup accessible (or protected) attributes for your model
  attr_accessible :email,
                  :password,
                  :password_confirmation,
                  :remember_me,
                  :provider,
                  :url,
                  :username,
                  :vk_id
                  
                  # @return [Client] Возвращает ORM-объект Клиента по токену вк
  def self.find_for_vkontakte_oauth access_token
    if client = VkClient.where(:url => access_token.info.urls.Vkontakte).first
      client
    else
        puts '----------------------------------'
      puts access_token
      puts '-----------------------------------'
      VkClient.create!(:url => access_token.info.urls.Vkontakte,
                     :username => access_token.info.name,
                     :email => access_token.uid+'@vk.com',
                     :vk_id => access_token.uid.to_i,
                     :password => Devise.friendly_token[0, 20])
    end
  end
end
