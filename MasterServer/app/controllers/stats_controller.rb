class StatsController < ApplicationController
  before_action :logged_in_user
  def index
    @filterrific = initialize_filterrific(
        Stat,
        params[:filterrific],
        select_options: {
            sorted_by: Stat.options_for_sorted_by
        },
        persistence_id: 'shared_key',
        default_filter_params: {},
        available_filters: [:sorted_by,:with_first_gte,:with_last_lt],
    ) or return
    @stats = @filterrific.find.page(params[:page])
    # Respond to html for initial page load and to js for AJAX filter updates.
    # respond_to do |format|
    #   format.html
    #   format.js
    # end
      # Recover from invalid param sets, e.g., when a filter refers to the
      # database id of a record that doesn’t exist any more.
      # In this case we reset filterrific and discard all filter params.
    respond_to do |format|
      format.html
      format.js
    end
  rescue ActiveRecord::RecordNotFound => e
    # There is an issue with the persisted param_set. Reset it.
    puts "Had to reset filterrific params: #{ e.message }"
    redirect_to(reset_filterrific_url(format: :html)) and return
  end


end
