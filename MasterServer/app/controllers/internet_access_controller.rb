require 'digest'
require_relative '../../lib/sms/smsc_api'
require_relative '../../lib/vk/vkhelper'

class InternetAccessController < ApplicationController
  include InternetAccessHelper
  before_action :check_cookie_expiration, only: [:authorized_template, :phone_template, :send_phone, :code_template, :send_code]
  before_action :is_a_client?, only: [:phone_template, :code_template, :authorized_template, :send_code, :send_phone]
  before_action :is_banned?, only: [:authorized_template, :phone_template, :code_template, :send_code, :send_phone]
  before_action :is_authorized, only: [:phone_template, :code_template, :send_code, :send_phone]
  before_action :check_session_auth_existing, only: [:code_template, :send_code]

  def set_post_template
    
  end    

 
  def not_a_client_template

  end
  
  def authorized_template
    wifi_session = Session.find_by_id(session[:wifi_session_id])
    @vk_client = VkClient.find_by_clients_id(wifi_session.client_id)
    owner_id, post_id = VKHelper.get_post_address
    @owner_id = owner_id
    @post_id = post_id
    app_id = '5586928'
    secret_key = 'LCqMnNiOOmkOaOnJ0r0l'
    @hash = nil
    unless @vk_client.nil?
      puts '----------------------------------->'
      puts 
      puts '----------------------------------->'
      @hash = Digest::MD5.hexdigest(app_id+@vk_client.email.split('@')[0]+secret_key)
    end
  end
  
  def banned_template
    
  end

  def phone_template

  end

  def send_phone
    auth_code_duration = 5 # minutes
    code_send_ban = 3 # minutes
    default_group = 'default_group'
    @phone = formatPhoneFromMaskedInputToNumbers params[:phone]
    if (@phone =~ /\d{10}/).nil?
      @form_error = params[:phone] + ' doesnt match'
      render 'phone_template'
    else
      wifi_session = Session.find_by_id(session[:wifi_session_id])
      session_auth = SessionAuth.find_by_session_id(wifi_session.id)
      client = Client.find_by(phone: params[:phone])
      if client.nil?
        puts '--------------------------'
        puts 'TRY TO CREATE CLIENT'
        group_policy = GroupPolicy.find_by_name(default_group)
        client = Client.create(phone: params[:phone], group_policy_id: group_policy.id, speed_limit_kbs: group_policy.speed_limit_kbs)
        client.save 
        puts '---------------------------'
      elsif client.is_banned
        @form_error = 'This phone is banned'
        render 'phone_template'
      end
      wifi_session.client_id = client.id
      wifi_session.status = 'unauthorized'
      wifi_session.save
      auth_code = generate_auth_code
      begin_time = Time.now
      end_time = begin_time + (60 * auth_code_duration)
      if session_auth.nil?
        session_auth = SessionAuth.create(session_id: wifi_session.id, code: auth_code, code_start_time: begin_time, code_end_time: end_time, sms_counter: 2, last_sms_sent_time: Time.now)
        send_sms(params[:phone], auth_code, session_auth.id)
        session[:code_tries] = 4
        redirect_to '/captive/auth/code'
      else
        if session_auth.sms_counter > 0
          session_auth.update(code: auth_code, code_start_time: begin_time, code_end_time: end_time, sms_counter: session_auth.sms_counter - 2, last_sms_sent_time: Time.now)
          send_sms(params[:phone], auth_code, session_auth.id)
          session[:code_tries] = 4
          redirect_to '/captive/auth/code'
        else
          if session_auth.last_sms_sent_time + code_send_ban * 60 > Time.now
            @form_error = 'Вы превысили количество возможных смс для отправки. Попробуйте снова через ' + code_send_ban.to_s + ' минуты.'
            render 'phone_template'
          else
            session_auth.update(code: auth_code, code_start_time: begin_time, code_end_time: end_time, sms_counter: 2, last_sms_sent_time: Time.now)
            send_sms(params[:phone], auth_code, session_auth.id)
            session[:code_tries] = 4
            redirect_to '/captive/auth/code'
          end
        end
      end
    end
  end

  def code_template

  end

  def send_code
    session_length = 24 # hours
    session_auth = SessionAuth.find_by_session_id(session[:wifi_session_id])
    if (params[:code] =~ /\d{4}/).nil? || session_auth.code.to_s != params[:code]
      case session[:code_tries]
        when 4
          @form_error = 'Неверный код.'
          session[:code_tries] -= 1
          render 'code_template'
        when 3
          @form_error = 'Неверный код.'
          session[:code_tries] -= 1
          render 'code_template'
        when 2
          session[:code_tries] -= 1
          @form_error = 'Неверный код.'
          render 'code_template'
        when 1
          session[:code_tries] -= 1
          session_auth.code_end_time = Time.now - 1
          session_auth.save
          redirect_to controller: 'internet_access', action: 'phone_template', code_error: 'Вы исчерпали количество попыток. Попробуйте еще раз.'
      end
    else
      parameters = Hash.new
      wifi_session = Session.find_by_id(session[:wifi_session_id])
      client = Client.find(wifi_session.client_id)
      wifi_session.beginning_time = Time.now
      wifi_session.end_time = Time.now + 60 * 60 * session_length
      wifi_session.status = 'active'
      parameters[:ip] = wifi_session.ip
      parameters[:bandwidth] = client.speed_limit_kbs
      parameters[:minutes] = (wifi_session.end_time - wifi_session.beginning_time) / 60
      write_action('set', parameters)
      wifi_session.save
      session_auth.delete
      redirect_to '/captive/authorized'
    end
  end

  private

  def is_banned?
    wifi_session_id = session[:wifi_session_id]
    unless wifi_session_id.nil?
      wifi_session = Session.find_by_id(wifi_session_id)
      client = Client.find_by_id(wifi_session.client_id)
      unless client.nil?
        if client.is_banned
          redirect_to '/captive/banned'
        end
      end
    end
  end  
  
  def is_phone_banned?(phone)
    client = Client.find_by_phone(phone)
    unless client.nil?
      if client.is_banned
         return true 
      end
    end
    return false
  end

  def is_a_client?
    puts '-----------------'
    puts 'CHECKING IS A CLIENT'
    puts '-----------------'
    if session[:wifi_session_id].nil? && params[:code].nil?
      redirect_to '/captive'
    elsif session[:wifi_session_id].nil? && !params[:code].nil?
      wifi_session = Session.find_by(code: params[:code])
      if wifi_session.nil?
        redirect_to '/captive'
      else
        set_wifi_session_id(wifi_session.id)
      end
    elsif !session[:wifi_session_id].nil? && params[:code].nil?
      wifi_session = Session.find(session[:wifi_session_id])
      if wifi_session.nil?
        session[:wifi_session_id] = nil
        redirect_to '/captive'
      end
    else
      cookie_wifi_session = Session.find(session[:wifi_session_id])
      params_wifi_session = Session.find_by_code(params[:code])
      if !cookie_wifi_session.nil? && !params_wifi_session.nil?
        if cookie_wifi_session.id != params_wifi_session.id
          set_wifi_session_id(params_wifi_session.id)
        end
      end
    end
  end

  def check_session_auth_existing
    puts '-----------------'
    puts 'CHECKING SESSION AUTH EXISTING'
    puts '-----------------'
    session_auth = SessionAuth.where("session_id = ? AND code_end_time > ?", session[:wifi_session_id], Time.now)
    if session_auth.empty?
      redirect_to controller: 'internet_access', action: 'phone_template', code_error: 'Срок действия кода истек.'
    end
  end

  def generate_auth_code
    rand = Random.new
    rand.rand(1000..9999)
  end

  def send_sms(phone, code, session_auth_id)
    phone = '7' + phone
    smsc = SMSC.new
    result = smsc.send_sms(phone, 'Your code is ' + code.to_s)
    puts result
    session_auth = SessionAuth.find_by_id(session_auth_id)
    session_auth.sms_counter += 1
    session_auth.last_sms_sent_time = Time.now
    session_auth.save
  end

  def is_authorized
    puts '-----------------'
    puts 'CHECKING SESSION AUTHORIZATION'
    puts '-----------------'
    wifi_session = Session.find_by_id(session[:wifi_session_id])
    if wifi_session.status == 'active'
      redirect_to '/captive/authorized'
    end
  end

  def set_wifi_session_id(wifi_session_id)
    session[:wifi_session_id] = wifi_session_id
    session[:expired_at] = Time.now + 60 * 60 * 24
  end

  def check_cookie_expiration
    puts '-----------------'
    puts 'CHECKING COOKIE EXPIRATION'
    puts '-----------------'
    unless session[:wifi_session_id].nil?
      if session[:expired_at] < Time.current
        session[:wifi_session_id] = nil
      end
    end
  end

  def write_action(type, parameters)
    action = Action.new
    action.action_type = type
    case type
      when 'set'
        action.ip = parameters[:ip]
        action.bandwidth = parameters[:bandwidth]
        action.minutes = parameters[:minutes]
      when 'unset'
        action.ip = parameters[:ip]
    end
    action.save
  end

end
