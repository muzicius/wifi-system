class InternalController < ApplicationController
  skip_before_filter :verify_authenticity_token
  def new_session
    puts '------------------------------'
    puts 'HERE IS PARAMS'
    puts params
    # {"codes"=>[{"ip"=>"10.10.10.5", "mac"=>"08:00:27:a7:ca:b7", "code"=>"73616"}], "controller"=>"internal", "action"=>"new_session", "token"=>"lolabc", "internal"=>{"codes"=>[{"ip"=>"10.10.10.5", "mac"=>"08:00:27:a7:ca:b7", "code"=>"73616"}]}}
    puts '------------------------------'
    response = Hash.new
    codes = params[:codes]
    if codes.nil?
      response[:status] = 'CODES EMPTY'
      render json: response
    end
    if is_token_valid(params[:token])
      codes.each do |code_hash|
        puts code_hash
        if Session.find_by(ip: code_hash[:ip], code: code_hash[:code], status: nil).nil?
          puts 'SESSION CREATE'
          Session.create(ip: code_hash[:ip], beginning_time: nil, end_time: nil, code: code_hash[:code])
        end
      end
      response[:status] = 'OK'
      render json: response
    else
      response[:status] = 'WRONG_TOKEN'
      render json: response
    end
  end

  def pull_updates
    puts '------------------------------'
    puts 'HERE IS PULL'
    puts '------------------------------'
    token = params[:token]
    response = Hash.new
    updates = Array.new
    if is_token_valid(token)
      Action.all.each do |action|
        temp = Hash.new
        temp[:params] = Hash.new
        temp[:action] = action.action_type
        case action.action_type
          when 'set'
            temp[:params][:ip] = action.ip
            temp[:params][:bandwidth] = action.bandwidth
            temp[:params][:minutes] = action.minutes
          when 'unset'
            temp[:params][:ip] = action.ip
          when 'ban_url'
            temp[:params][:url] = action.url
          when 'unban_url'
            temp[:params][:url] = action.url
        end
        updates.push(temp)
      end
      response[:status] = 'ok'
      response[:updates] = Array.new
      response[:updates].push(:actions => updates)
    else
      response[:status] = 'fail'
    end
    puts response
    render json: response
  end

  def push_events
    puts '----------------------------'
    puts 'PUSH EVENTS IS HERE'
    puts params
    puts '----------------------------'
    response = Hash.new
    if is_token_valid(params[:token])
      params[:events].each do |event|
        type = event[:event]
        parameters = event[:params]
        case type
          when 'on_set_succeeded'
            Action.where(ip: parameters[:ip], :action_type => 'set').delete_all
          when 'on_unset_succeeded'
            wifi_session = Session.find_by(ip: parameters[:ip], status: 'active')
            wifi_session.status = 'closed'
            wifi_session.save
            Action.where(ip: parameters[:ip], :action_type => 'unset').delete_all
          when 'on_ban_url_succeeded'
            Action.where(url: parameters[:url], :action_type => 'ban_url').delete_all
          when 'on_unban_url_succeeded'
            Action.where(url: parameters[:url], :action_type => 'unban_url').delete_all
        end
      end
      response[:status] = 'ok'
    else
      response[:status] = 'fail'
    end
    render json: response
  end

  private
  def is_token_valid(token)
    authorized_app = AuthorizedApp.find_by_token(token)
    authorized_app != nil
  end

end

