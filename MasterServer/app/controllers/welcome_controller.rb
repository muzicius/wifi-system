class WelcomeController < ApplicationController
  before_action :logged_in_user, only: [:index, :delete ]

  # @return [Array] [srcaddr,dstaddr,port,srcport,dstport,first,last]
  def index
    @recs = Session.where('status =?', 'active')
    # @recs = Session.all
    @records = @recs.paginate(page: params[:page],per_page: 20).order('end_time DESC')
  end
  def delete
    puts params[:ip]
    session = Session.find_by(ip: params[:ip])
    session.update(status:'closed',end_time: session[:beginning_time])
    WelcomeController.write_action('unset',{ip:params[:ip]})
    redirect_to root_url
  end
  def changeSpeed
    session = Session.find_by(ip:params[:ip])
    time = session.end_time - Time.now
    WelcomeController.write_action('set',{ip:params[:ip],bandwidth:params[:speed],minutes: time})
    redirect_to root_url
  end

  def vk
  end

  def clients
    @recs = Session.all
    @records = @recs.paginate(page: params[:page],per_page: 20).order('ip DESC')
  end

  def set_vk_post
    begin
      set_post(params[:vk_link])
    rescue 
      flash.now[:danger] = 'Invalid VK post'
    end
    render 'vk'
  end
  def self.write_action(type, parameters)
    action = Action.new
    action.action_type = type
    case type
      when 'set'
        action.ip = parameters[:ip]
        action.bandwidth = parameters[:bandwidth]
        action.minutes = parameters[:minutes]
      when 'unset'
        action.ip = parameters[:ip]
    end
    action.save
  end
end
