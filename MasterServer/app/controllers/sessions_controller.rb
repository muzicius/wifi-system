class SessionsController < ApplicationController
  def new
  end

  # Создает новую сессию
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in user
      # debugger
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_to root_url
    else
      # Create an error message.
      flash.now[:danger] = 'Invalid email/password combination'
      redirect_to login_url

    end
  end
  # Confirms a logged-in user.

  # Удалает сессию
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
