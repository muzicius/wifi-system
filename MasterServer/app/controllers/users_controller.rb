class UsersController < ApplicationController
  before_action :check_supervisor?, only: [:create,:new]
  def new
    @client = User.new
  end

  # Добавляет нового пользователя
  def create
    @client = User.new(user_params)
    if @client.save
      flash[:success] = "Welcome to the Sample App!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  # Возвращает поьзователя по ID
  # @return User
  def show
  @client = User.find(params[:id])
    # debugger
  end

  private
  def user_params
    params.require(:user).permit(:email, :password,
                                   :password_confirmation, :role_id)
  end
  
  # Проверяет, является ли пользователь администраторов
  # @return [Boolean] true - если является
  def check_supervisor?
    if supervisor? then
      true
    else
      # redirect_to root_url
      redirect_back_or login_url
    end
  end

end
