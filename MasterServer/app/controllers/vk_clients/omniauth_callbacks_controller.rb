class VkClients::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def vkontakte
      puts "------>HERE<---------"
      puts session[:wifi_session_id]
    @client = VkClient.find_for_vkontakte_oauth request.env["omniauth.auth"]
    if @client.persisted?
      puts '------------------'
      puts 'OKAY'
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Vkontakte"
      #sign_in_and_redirect @client, :event => :authentication 
      wifi_session = Session.find_by_id(session[:wifi_session_id])
      @client.clients_id = wifi_session.client_id
      @client.save!
      sign_in @client
      redirect_to '/captive/authorized'
    else
      puts '------------------'
      puts 'ERROR'
      flash[:notice] = "authentication error"
      redirect_to root_path
    end
  end
end