module InternetAccessHelper
  def formatPhoneFromMaskedInputToNumbers(phone)
    phone.gsub(/[\s()-]/,'')   # delete brackets,space and "-"
  end
end
