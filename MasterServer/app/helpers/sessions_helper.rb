module SessionsHelper
  # Вход по ID пользователя
  # @param [User] user - ActiveRecord объект пользователя
  def log_in(user)
    session[:user_id] = user.id
  end
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # Проверяет, авторизовн текущий пользователь или нет?
  # @return [Boolean] true - если вход выполнен
  def logged_in?
    !current_user.nil?
  end

  # Выполняет выход для текущего пользователя
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil

  end

  # Запоминает пользователя в постоянную сессию.
  # @param [User] user - ActiveRecord объект пользователя
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # Удаляет пользователя из постоянной сессии.
  # @param [User] user - ActiveRecord объект пользователя
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  # Проверяет, является ли текущий пользователь администратором
  # @return [Boolean] true, если пользователь - администратор
  def supervisor?
    (!current_user.nil? && current_user[:role_id] == 1) ? true : false
  end

  # Redirects to stored location (or to the default).
  # TODO: Так, шо вот эта? Шо за параметр default???
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  # TODO: И вот это перевидите плз
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
end

