require_relative '../../lib/vk/vkhelper'
module ApplicationHelper

    # Возвращает полный заголовок, основываясь на пагинации.
    # @return [String] полный заголовок
    def full_title(page_title = '')
      base_title = "Innopolis WiFi"
      if page_title.empty?
        base_title
      else
        page_title + " | " + base_title
      end
    end
    
    def set_post(link)
      VKHelper.post_to_check = link
    end
end
