#!/usr/bin/python
import Queue
import time

from application.components.ip_manager import IPManager
from application.components.update_dealer import UpdateDealer
from application.components.web_server import WebServer
from application.config import SETTINGS

from application.helpers.exceptions import IPManagerException, WebServerException, UpdateDealerException

PANEL_CLIENT_TOKEN = SETTINGS['app']['panel_client_token']
DELAY = SETTINGS['app']['delay']


def start_ip_manager(events_bucket, exceptions_bucket):
    im = IPManager(events_bucket, exceptions_bucket)
    im.start()

    return im


def start_web_server(codes_bucket, exceptions_bucket):
    ws = WebServer(codes_bucket, exceptions_bucket)
    ws.start()

    return ws


def start_update_dealer(codes_bucket, events_bucket, exceptions_bucket):
    updates_out_queue = Queue.Queue()
    requests_in_queue = Queue.Queue()

    pc = UpdateDealer(PANEL_CLIENT_TOKEN,
                      requests_in_queue,
                      updates_out_queue,
                      codes_bucket,
                      events_bucket,
                      exceptions_bucket)
    pc.start()

    return pc, updates_out_queue, requests_in_queue


def do_tick(ip_manager, ws, pc, updates_out_queue, requests_in_queue):
    if not updates_out_queue.empty():
        print('>>> 4 PROCESS UPDATES')

        try:
            update = updates_out_queue.get()

            for action in update['actions']:
                if action['action'] == 'set':
                    ip_manager.set_ip(action['params']['ip'], action['params'])
                elif action['action'] == 'unset':
                    ip_manager.unset_ip(action['params']['ip'])
                elif action['action'] == 'ban_url':
                    ip_manager.ban_url(action['params']['url'])
                elif action['action'] == 'unban_url':
                    ip_manager.unban_url(action['params']['url'])
        except Queue.Empty:
            pass


def main_loop():
    exceptions_bucket = Queue.Queue() # to handle thread crashes
    codes_bucket = Queue.Queue() # codes to be sent to the master-server
    events_bucket = Queue.Queue() # events to be sent to the master-server

    web_server = start_web_server(codes_bucket, exceptions_bucket) # web server thread
    update_dealer, updates_out_queue, requests_in_queue = start_update_dealer(codes_bucket, events_bucket, exceptions_bucket)
    ip_manager = start_ip_manager(events_bucket, exceptions_bucket)

    running = True

    while running:
        do_tick(ip_manager, web_server, update_dealer, updates_out_queue, requests_in_queue)

        if exceptions_bucket.empty():
            continue

        try:
            exception = exceptions_bucket.get(block=False)
        except Queue.Empty:
            continue
        else:
            if isinstance(exception, IPManagerException):
                if ip_manager is not None:
                    ip_manager.kill()

                ip_manager = start_ip_manager(events_bucket, exceptions_bucket)
            elif isinstance(exception, WebServerException):
                if web_server is not None:
                    web_server.kill()

                web_server = start_web_server(codes_bucket, exceptions_bucket)
            elif isinstance(exception, UpdateDealerException):
                if update_dealer is not None:
                    update_dealer.kill()

                update_dealer, updates_out_queue, requests_in_queue = start_update_dealer(codes_bucket, events_bucket, exceptions_bucket)

        try:
            time.sleep(DELAY)
        except:
            running = False


if __name__ == '__main__':
    main_loop()
