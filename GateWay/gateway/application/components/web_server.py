import bottle
from bottle import run as run_bottle, redirect, request
from application.config import SETTINGS
from application.db_manager import PanelCodeManager
from application.helpers.exceptions import WebServerException

from application.helpers.kthread import KThread

from application.helpers import ip2mac


class WebServer(KThread):
    HOST = SETTINGS['web_server']['host']
    PORT = SETTINGS['web_server']['port']
    REDIRECT_URL = SETTINGS['web_server']['redirect_url_test']

    def __init__(self, codes_bucket, exceptions_bucket):
        KThread.__init__(self)
        self._codes_bucket = codes_bucket
        self._exceptions_bucket = exceptions_bucket

    def index(self):
        ip = request.environ.get('REMOTE_ADDR')
        code = PanelCodeManager.get_code(ip)
        macs = ip2mac.ips_to_macs([ip])

        print('MACS: %s' % str(macs))

        code_packet = {'ip': ip, 'code': code}

        if len(macs) > 0:
            code_packet['mac'] = macs[0]

        self._codes_bucket.put(code_packet)

        print('>>> 0 INDEX !!!')

        return redirect(WebServer.REDIRECT_URL % {'code': code})

    def run(self):
        try:
            bottle.route('/')(self.index)

            run_bottle(host=WebServer.HOST, port=WebServer.PORT)
        except:
            self._exceptions_bucket.put(WebServerException())

