import datetime
import time
from abc import abstractmethod

import subprocess
from application.config import SETTINGS
from application.db_manager import Job, PanelCodeManager
from application.helpers.exceptions import IPManagerException
from pony.orm import db_session, select

from application.helpers.kthread import KThread
from application.helpers.utils import datetime_to_timestamp


SHELL_SCRIPT_ADD = '/root/scripts/uni_wifi/gateway/shell_scripts/manage_host.sh'
SHELL_SCRIPT_BAN = '/root/scripts/uni_wifi/gateway/shell_scripts/ban.sh'
SHELL_PATH = '/usr/bin/sh'


def is_ip_valid(ip):
    if not isinstance(ip, str):
        return False

    ip_segments = ip.split('.')

    check_segment = lambda segment: str(segment).isdigit() and int(segment) <= 255 and int(segment) >= 0
    check_segments = lambda segments: len(segments) == 4 and reduce(lambda res, next: res and next,
                                                                    map(check_segment, ip_segments), True)
    return check_segments(ip_segments)


class AbstractPerformer():
    def __init__(self, events_bucket):
        self._events_bucket = events_bucket

    '''
    Perform an action on schedule tick

    :param str ip: client IP
    :param str action: action to perform
    :param dict params: additional parameters to be passed to action
    '''

    @abstractmethod
    def perform(self, ip, action, url, params):
        pass


class Scheduler():
    TIMEOUT = 1

    '''
    :param performer - the one responsible for jobs execution
    '''

    def __init__(self, performer):
        self.performer = performer

    def set_job(self, ip, action, when_to_run):
        with db_session:
            print('>>> 10 TRY TO GET JOB')
            job = Job.get(ip=ip)

            if job == None:
                print('>>> 11 CREATE NEW JOB')
                job = Job(ip=ip, action=action, when_to_run=when_to_run)
            else:
                print('>>> 11 UPDATE EXISTING JOB')
                job.set(ip=ip, action=action, when_to_run=when_to_run)

        pass

    def remove_job(self, ip):
        with db_session:
            print('>>> 14 GET JOB')
            job = Job.get(ip=ip)

            if job != None:
                print('>>> 15 REMOVE JOB')
                job.delete()
            else:
                print('>>> 15 DO NOTHING')

        pass

    def process_jobs(self):
        now = datetime.datetime.now()

        with db_session:
            jobs = select(job for job in Job if job.when_to_run <= now).order_by(Job.when_to_run)[:]

            for job in jobs:
                print('[%s]: %s (%s)' % (job.ip, job.action, job.when_to_run))
                self.performer.perform(job.ip, job.action, None)
                job.delete()

        pass


class IPManagementPerformer(AbstractPerformer):
    def __init__(self, events_bucket):
        AbstractPerformer.__init__(self, events_bucket)

    def perform(self, ip, action, url=None, params={}):
        if not is_ip_valid(ip) and url == None:
            self.ON_ACTION_FAILED(action, error='Invalid params')
            return False

        when_timestamp = datetime_to_timestamp(datetime.datetime.now())
        result = False

        if action == 'set':
            bandwidth = params.get('bandwidth', None)
            minutes = params.get('minutes', None)

            if bandwidth is None or minutes is None or \
                    not str(bandwidth).isdigit() or not str(minutes).isdigit():
                self.ON_ACTION_FAILED(action, error='Invalid params')
                return False

            result = self._auth_ip(ip, {'minutes': int(minutes), 'bandwidth': int(bandwidth)})

            if result:
                self.ON_ACTION_SUCCEEDED(action, ip=ip, when_timestamp=when_timestamp)
            else:
                self.ON_ACTION_FAILED(action, ip=ip, when_timestamp=when_timestamp)
        elif action == 'unset':
            result = self._unauth_ip(ip)

            if result:
                self.ON_ACTION_SUCCEEDED(action, ip=ip, when_timestamp=when_timestamp)
            else:
                self.ON_ACTION_FAILED(action, ip=ip, when_timestamp=when_timestamp)
        elif action == 'ban_url':
            result = self._ban_url(url)

            if result:
                self.ON_ACTION_SUCCEEDED(action, url=url, when_timestamp=when_timestamp)
            else:
                self.ON_ACTION_FAILED(action, url=url, when_timestamp=when_timestamp)
        elif action == 'unban_url':
            result = self._unban_url(url)

            if result:
                self.ON_ACTION_SUCCEEDED(action, url=url, )
            else:
                self.ON_ACTION_FAILED(action, url=url, when_timestamp=when_timestamp)

        return result

    def _exec_shell_command(self, args):
        try:
            out = subprocess.check_output(args)
            return 0, out
        except subprocess.CalledProcessError as exec_result:
            return exec_result.returncode, exec_result.output

    def _ON_ACTION_PERFORMED(self, status, action, **params):
        event = {'event': 'on_%s_%s' % (action, status)}
        event['params'] = params

        self._events_bucket.put(event)

    def ON_ACTION_FAILED(self, action, **params):
        self._ON_ACTION_PERFORMED('failed', action, **params)

    def ON_ACTION_SUCCEEDED(self, action, **params):
        self._ON_ACTION_PERFORMED('succeeded', action, **params)

    def _unauth_ip(self, ip):
        # TODO: FIX
        # TODO: DO NOT REMOVE TILL MASTER SERVER CONFIRMED
        exit_code, out = self._exec_shell_command([SHELL_PATH, SHELL_SCRIPT_ADD, "del", ip])
        PanelCodeManager.remove_code(ip)
        print('UNAUTH, OUT: %s' % exit_code)

        print('>>> ## PUT ON_UNSET_SUCCEED EVENT')

        return exit_code == 0

    def _auth_ip(self, ip, params):
        # TODO: FIX
        bandwidth = str(params['bandwidth'])
        exit_code, out = self._exec_shell_command([SHELL_PATH, SHELL_SCRIPT_ADD, "add", ip,
                                        bandwidth, bandwidth])
        print('>>> 7 AUTH, OUT: %s (%s)' % (exit_code, datetime.datetime.now()))

        print('>>> 8 PUT EVENT')

        return exit_code == 0

    def _ban_url(self, url):
        exit_code, out = self._exec_shell_command([SHELL_PATH, SHELL_SCRIPT_BAN, "ban", url])
        print('>>> BAN URL: %s, exit_code: %s' % (url, exit_code))
        return exit_code == 0

    def _unban_url(self, url):
        exit_code, out = self._exec_shell_command([SHELL_PATH, SHELL_SCRIPT_ADD, "unban", url])
        print('>>> UNBAN URL: %s, exit_code: %s' % (url, exit_code))
        return exit_code == 0


class IPManager(KThread):
    '''
        Manages access rights of given IP addresses.
        It is triggered when
    '''
    DELAY = SETTINGS['ip_manager']['delay']

    def __init__(self, events_bucket, exceptions_bucket):
        KThread.__init__(self)
        self._exceptions_bucket = exceptions_bucket
        self._events_bucket = events_bucket

        self.performer = IPManagementPerformer(events_bucket)
        self.sched = Scheduler(self.performer)

    def set_ip(self, ip, params):
        ip = ip.encode('ascii')
        print('>>> 6 PERFORM SET OPERATION')

        if not self.performer.perform(ip, 'set', None, params):
            return False

        # TODO: params
        print('>>> 9 SCHEDULE REMOVAL JOB')
        self.sched.set_job(ip, 'unset', datetime.datetime.now() +
                           datetime.timedelta(minutes=params['minutes']))
        return True

    def unset_ip(self, ip):
        ip = ip.encode('ascii')
        print('>>> 12 PERFORM UNSET OPERATION')

        if not self.performer.perform(ip, 'unset'):
            return False

        print('>>> 13 REMOVE JOB FROM SCHEDULER')
        self.sched.remove_job(ip)
        return True

    def ban_url(self, url):
        if not self.performer.perform(None, 'ban_url', url=url):
            return False
        return True

    def unban_url(self, url):
        if not self.performer.perform(None, 'unban_url', url=url):
            return False
        return True

    def run(self):
        try:
            while True:
                self.sched.process_jobs()

                time.sleep(IPManager.DELAY)
        except:
            self._exceptions_bucket.put(IPManagerException())
