#!/usr/bin/python2
import Queue
import time

import requests
from application.config import SETTINGS
from application.helpers.exceptions import UpdateDealerException

from application.helpers.kthread import KThread


class Api():
    API_URL = SETTINGS['update_dealer']['api_url_test']

    def __init__(self, token):
        self.token = token
        self.api_url = '%s/%s' % (Api.API_URL, self.token)

    def _request(self, method_name, **data):
        request_url = '%s/%s' % (self.api_url, method_name)

        try:
            response = requests.post(request_url, json=data)
            return response.json()  # here it comes
        except:
            return None

    def pullUpdates(self, offset, limit):
        return self._request('pullUpdates', offset=offset, limit=limit)

    def pushEvents(self, **kwargs):
        return self._request('pushEvents', **kwargs)

    def pushCodes(self, codes):
        print('>>> 1 PUSH CODES')
        return self._request('pushCodes', **{'codes': codes})


class UpdateDealer(KThread):
    def __init__(self, token, in_queue, out_queue, codes_bucket, events_bucket, exceptions_bucket):
        KThread.__init__(self)

        self.api = Api(token)

        self._in_queue = in_queue
        self._out_queue = out_queue
        self._codes_bucket = codes_bucket
        self._events_bucket = events_bucket
        self._exceptions_bucket = exceptions_bucket

    def run(self):
        def push_events(events_bucket):
            events = []

            while events_bucket.qsize() > 0:
                try:
                    event = events_bucket.get(block=False)
                except Queue.Empty:
                    break
                else:
                    events.append(event)

            if len(events) > 0:
                print('>>> $$$ PUSH EVENTS')
                self.api.pushEvents(events=events)

        def push_codes(codes_bucket):
            codes = []

            while codes_bucket.qsize() > 0:
                try:
                    code = codes_bucket.get(block=False)
                except Queue.Empty:
                    break
                else:
                    codes.append(code)

            if len(codes) > 0:
                print('>>> $$$ PUSH CODES')
                self.api.pushCodes(codes=codes)

        try:
            while True:
                push_codes(self._codes_bucket)
                push_events(self._events_bucket)

                updates = self.api.pullUpdates(0, 0)['updates']

                if len(updates) < 1:
                    continue
                else:
                    print('>>> 3 PULL UPDATES')

                for update in updates:
                    # print('UPDATE: %s' % update)
                    self._out_queue.put(update)

                time.sleep(5)
        except:
            self._exceptions_bucket.put(UpdateDealerException())

