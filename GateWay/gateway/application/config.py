SETTINGS = {
    'web_server': {
        'host': '0.0.0.0',
        'port': 8080,
        'redirect_url_test': 'http://localhost:4567/captive/auth?code=%(code)s',
        'redirect_url': 'http://10.90.104.46:4567/captive/auth?code=%(code)s'
    },
    'update_dealer': {
        'api_url_test': 'http://localhost:4567/api',
        'api_url': 'http://10.90.104.46:4567/api'
    },
    'ip_manager': {
        'delay': 0.7
    },
    'app': {
        'delay': 0.7,
        'panel_client_token': 'lolabc'
    }
}
