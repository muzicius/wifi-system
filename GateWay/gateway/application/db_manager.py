from datetime import datetime
from pony.orm import *
import random

db = Database("sqlite", "database.sqlite", create_db=True)

class Job(db.Entity):
    ip = PrimaryKey(str)
    action = Required(str, lazy=False)
    when_to_run = Required(datetime)


class PanelCode(db.Entity):
    ip = PrimaryKey(str)
    code = Required(str, unique=True)


class PanelCodeManager():
    '''
        Manage unique codes to attach to request url while redirecting to master-server.
    '''

    @staticmethod
    def _gen_random_val():
        return str(random.randint(10000, 99999))

    @staticmethod
    def _gen_code():
        code = PanelCodeManager._gen_random_val()

        with db_session:
            panelCode = PanelCode.get(code=code)

            while panelCode != None:
                code = PanelCodeManager._gen_random_val()
                panelCode = PanelCode.get(code=code)

            return code

    @staticmethod
    def get_code(ip):
        with db_session:
            panel_code = PanelCode.get(ip=ip)

            if panel_code == None:
                code = PanelCodeManager._gen_code()
                panel_code = PanelCode(ip=ip, code=code)

            return panel_code.code

    @staticmethod
    def remove_code(ip):
        with db_session:
            panel_code = PanelCode.get(ip=ip)

            if panel_code != None:
                panel_code.delete()


def setup():
    # sql_debug(True)
    db.generate_mapping(create_tables=True)
    pass


setup()
