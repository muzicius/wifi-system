from threading import Thread

import sys

class KillThreadException(Exception): pass


class KThread(Thread):
    '''
        Killable thread
    '''
    def __init__(self, *args, **keywords):
        Thread.__init__(self, *args, **keywords)
        self.daemon = True
        self.killed = False

    def start(self):
        """Start the thread."""
        self.__run_backup = self.run
        self.run = self.__run # Force the Thread to install our trace.
        Thread.start(self)

    def __run(self):
        """Hacked run function, which installs the
        trace."""
        sys.settrace(self.globaltrace)
        try:
            self.__run_backup()
        except:
            print('wert')
            pass

        try:
            self.run = self.__run_backup
        except:
            pass

    def globaltrace(self, frame, why, arg):
        if why == 'call':
            return self.localtrace
        else:
            return None

    def localtrace(self, frame, why, arg):
        if self.killed:
            if why == 'line':
                raise KillThreadException()
                # pass
        return self.localtrace

    def kill(self):
        self.killed = True