#!/usr/bin/python

import subprocess
import sys


def ips_to_macs(target_ips):
    # command to be executed for retrieval of MAC by IP
    print('IPS: %s' % str(target_ips))

    CMD = 'arp'

    # read command output

    output = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE).stdout.read()

    # parse output

    entries = output.split('\n')

    def unpack_mac(entry):
        entry_parts = entry.split()

        if len(entry_parts) >= 2:
            return entry_parts[2]
        else:
            return None

    target_macs = []

    for target_ip in target_ips:
        target_macs.extend( map(unpack_mac, filter(lambda entry: entry.startswith(target_ip), entries)) )

    return target_macs


if __name__ == '__main__':
    # be sure that ip has been passed to the program

    if len(sys.argv) < 2:
        print('Usage: ip2mac.py <IP_ADDRESS> [<IP_ADDRESS_2> .. <IP_ADDRESS_N>]\n')
        print('Examples:')
        print('\t./ip2mac.py 10.90.104.17\t\t -- for single IP')
        print('\t./ip2mac.py 10.90.104.17 10.90.104.103\t -- for multiple IPs')
        sys.exit(2)

    target_ips = sys.argv[1:]

    target_macs = ips_to_macs(target_ips)

    for target_mac in target_macs:
        print(target_mac)
