import time
import datetime


def timestamp_to_datetime(timestamp):
    return datetime.datetime.fromtimestamp(timestamp)


def datetime_to_timestamp(datetime):
    dt = datetime.fromtimestamp(1346236702)

    return time.mktime(dt.timetuple())

