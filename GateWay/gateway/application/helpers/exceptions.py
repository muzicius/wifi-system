class IPManagerException(Exception):
    pass

class UpdateDealerException(Exception):
    pass

class WebServerException(Exception):
    pass
